fetch('./data.json').then(response => response.json()).then(data => {
    chartForMatchesPerSeason(data["MatchesPlayed"]);
    barChartForNoOfWins(data["MatchesWonPerTeamPerYear"]);
    chartForExtraRunsPerTeam(data["ExtraRunsPerTeam"]);
    chartForEconomicalBowlers(data["EconomicalBowler"]);
})

function formatdataForColumnChart(object) {
   //write your code to convert jsondata for the column chart format.
}

function formatdataForBarChart(object) {
    //write your code to convert jsondata for the Bar chart format.
}

function chartForMatchesPerSeason(jsonData){
//complete this function to create visualization for no.ofmathches per season.
}
function barChartForNoOfWins(jsonData){
//complete this function to create visualization for no.ofWins per team per season.   
}

function chartForExtraRunsPerTeam(jsonData){
//complete this function to create visualization for extraruns per team for year 2016 .   
}

function chartForEconomicalBowlers(jsonData){
 //complete this function to create visualization for top ten economical bowler for year 2015 .   
}