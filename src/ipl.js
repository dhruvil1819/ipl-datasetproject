var csvToJson=require('./csvtojson')
var matchesData = csvToJson('./data/matches.csv')
var deliveryData = csvToJson('./data/deliveries.csv')
const getNoOfMatchesPlayed = () => {
  //write your code here
}
const getNoOfMatchesWonPerTeamPerYear = () => {
  //write your code here
}
const getExtraRunsPerTeamForYear = () => {
 //write your code here
}
const getEconomicalBowlersForYear = () => {
 //write your code here
}

module.exports.getNoOfMatchesPlayed = getNoOfMatchesPlayed
module.exports.getNoOfMatchesWonPerTeamPerYear = getNoOfMatchesWonPerTeamPerYear
module.exports.getExtraRunsPerTeamForYear = getExtraRunsPerTeamForYear
module.exports.getEconomicalBowlersForYear = getEconomicalBowlersForYear
